# FizzBuzz

Write a function to return "Fizz" if the function argument is divisible by 3, "Buzz" if divisible by 5, "FizzBuzz" if divisible by both and the the number (as a String) otherwise.

EXAMPLE

Input -> Output: 3 -> Fizz, 0 -> FizzBuzz, 17 -> 17, 25 -> Buzz
