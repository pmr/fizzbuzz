import kotlin.test.Test
import kotlin.test.assertEquals

class Test {

    @Test fun `verify zero and the first 1 billion positive integers`() {
        var threeCounter = 0
        var fiveCounter = 0
        fun getExpected(i: Int): String = when {
            threeCounter == 0 && fiveCounter == 0 -> "FizzBuzz"
            threeCounter == 0 && fiveCounter != 0 -> "Fizz"
            threeCounter != 0 && fiveCounter == 0 -> "Buzz"
            else -> i.toString()
        }

        for (i in 0..1_000_000_000) {
            assertEquals(getExpected(i), fizzBuzz(i))
            threeCounter++
            fiveCounter++
            if (threeCounter == 3) threeCounter = 0
            if (fiveCounter == 5) fiveCounter = 0
        }
    }
}
