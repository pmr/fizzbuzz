
fun fizzBuzz(value: Int): String = when {
    value % 15 == 0 -> "FizzBuzz"
    value % 3 == 0 -> "Fizz"
    value % 5 == 0 -> "Buzz"
    else -> value.toString()
}
